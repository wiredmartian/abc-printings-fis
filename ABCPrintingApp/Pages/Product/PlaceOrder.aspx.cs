﻿using ABCPrintingApp.Business;
using ABCPrintingApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ABCPrintingApp.Pages.Product
{
    public partial class PlaceOrder : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnTotal_Click(object sender, EventArgs e)
        {
            ProductsLogic l = new ProductsLogic();
            string logoText = txtPrint.Text;
            int charText = logoText.Length;
            int qty = Convert.ToInt16(txtQty.Text);
            int productId = Convert.ToInt16(Request.QueryString["productId"]);
            decimal total = l.CalcTotal(qty, charText, productId);
            lblTotal.Text = "Total Price: " + total.ToString("R0.00");
        }

        protected void btnOrder_Click(object sender, EventArgs e)
        {
            int productId = Convert.ToInt16(Request.QueryString["productId"]);
            ProductsLogic l = new ProductsLogic();
            Order order = new Order
            {
                Shirt_Wrting = txtPrint.Text,
                Customer_Email = txtEmail.Text,
                Customer_Phone = txtNumber.Text,
                Quantity = Convert.ToInt16(txtQty.Text)
            };
            Order o = l.PlaceOrder(order, productId);
            Response.Redirect("~/Pages/Product/OrderDetails.aspx?OrderId=" + o.OrderID);
        }
    }
}