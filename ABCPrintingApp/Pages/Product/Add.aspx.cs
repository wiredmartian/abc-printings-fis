﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ABCPrintingApp.Models;
using ABCPrintingApp.Business;

namespace ABCPrintingApp.Pages.Product
{
    public partial class Add : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            HttpPostedFile image = HttpContext.Current.Request.Files["productImage"];
            IProducts _product = new ProductsLogic();

            Models.Product p = new Models.Product
            {
                Name = txtName.Text,
                Color = ddlColor.SelectedValue,
                Available_QTY = Convert.ToInt32(txtQuantity.Text),
                Description = txtDesc.Text,
                Price = Convert.ToDecimal(txtPrice.Text),
            };
            string response = _product.AddProduct(p, image);
            lblMsg.Text = response;

        }
    }
}