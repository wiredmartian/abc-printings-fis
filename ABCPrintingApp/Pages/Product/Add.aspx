﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="ABCPrintingApp.Pages.Product.Add" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ABC Printing</title>
    <link href="../../Content/bootstrap.css" rel="stylesheet" />
    <link href="../../Content/bootstrap-theme.css" rel="stylesheet" />

    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 293px;
        }
        .auto-style3 {
            width: 293px;
            height: 40px;
        }
        .auto-style4 {
            height: 40px;
        }
        .auto-style5 {
            width: 293px;
            height: 43px;
        }
        .auto-style6 {
            height: 43px;
        }
        .auto-style7 {
            width: 293px;
            height: 42px;
        }
        .auto-style8 {
            height: 42px;
        }
        .auto-style9 {
            width: 293px;
            height: 37px;
        }
        .auto-style10 {
            height: 37px;
        }
        .auto-style11 {
            width: 293px;
            height: 36px;
        }
        .auto-style12 {
            height: 36px;
        }
    </style>
</head>
<body class="container">
      <div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">ABC Printing</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="All.aspx">Products</a></li>
              <li><a href="Add.aspx">Add</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
        </div>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <h2>Add Product</h2>
    <div>
        <br />
        <asp:Label ID="lblMsg" style="color:orangered" runat="server"></asp:Label>
        <br />
        <table class="auto-style1">
            <tr>
                <td class="auto-style3">Name</td>
                <td class="auto-style4">
                    <asp:TextBox ID="txtName" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style5">Color</td>
                <td class="auto-style6">
                    <asp:DropDownList ID="ddlColor" runat="server" CssClass="form-control dropdown">
                        <asp:ListItem>Black</asp:ListItem>
                        <asp:ListItem>White</asp:ListItem>
                        <asp:ListItem>Red</asp:ListItem>
                        <asp:ListItem>Orange</asp:ListItem>
                        <asp:ListItem>Navy</asp:ListItem>
                        <asp:ListItem>Blue</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td class="auto-style7">Description</td>
                <td class="auto-style8">
                    <asp:TextBox ID="txtDesc" runat="server" Rows="10" CssClass="form-control"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style9">Unit Price</td>
                <td class="auto-style10">
                    <asp:TextBox ID="txtPrice" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style11">Available Quantity</td>
                <td class="auto-style12">
                    <asp:TextBox ID="txtQuantity" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style11">Image</td>
                <td class="auto-style12">
                    <asp:FileUpload ID="productImage" runat="server" CssClass="form-control" />
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td>
                    <asp:Button ID="btnAdd" runat="server" Height="37px" OnClick="btnAdd_Click" CssClass="btn btn-primary" Text="Save Product" Width="136px" />
                </td>
            </tr>
        </table>
    
    </div>
    </form>
    <script src="../../Scripts/jquery-1.9.0.js"></script>
    <script src="../../Scripts/bootstrap.js"></script>
</body>
</html>
