﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OrderDetails.aspx.cs" Inherits="ABCPrintingApp.Pages.Product.OrderDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ABC Printing</title>
    <link href="../../Content/bootstrap.css" rel="stylesheet" />
    <link href="../../Content/bootstrap-theme.css" rel="stylesheet" />
</head>
<body class="container">
      <div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">ABC Printing</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="All.aspx">Products</a></li>
              <li><a href="Add.aspx">Add</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
        </div>
    <form id="form1" runat="server">
    <asp:FormView ID="productDetail" runat="server" ItemType="ABCPrintingApp.Models.Order" SelectMethod ="GetOrder" RenderOuterTable="false">
        <ItemTemplate>
            <div>
                <h1>Order #<%#:Item.OrderID %> - <%#:Item.Shirt_Wrting %></h1>
            </div>
            <br />
            <table>
                <tr>
                    <td>
                        <img src="data:image/jpeg;base64,<%#: Convert.ToBase64String(Item.Product.Image) %>" class="img-thumbnail img-responsive" style="height:300px" alt="<%#:Item.Product.Name %>" id="Imgsrc"/>
                    </td>
                    <td>&nbsp;</td>  
                    <td style="vertical-align: top; text-align:left;">
                        <b>Date:</b>&nbsp;<%#:Item.OrderDate.ToLongDateString() %><br />
                        <b>Quantity:</b>&nbsp;<%#:Item.Quantity %><br />
                        <span><b>Total Price:</b>&nbsp;<%#: String.Format("{0:c}", Item.Total_Price) %></span><br />
                        <span><b>Customer Email:</b>&nbsp;<%#:Item.Customer_Email %></span><br />
                        <span><b>Customer Phone:</b>&nbsp;<%#:Item.Customer_Phone %></span>
                        <br />
                        <br />
                        <span><a class="btn btn-default" href="All.aspx">All Products</a></span><br />
                    </td>
                    
                </tr>
            </table>
        </ItemTemplate>
    </asp:FormView>
    </form>
    <script src="../../Scripts/jquery-1.9.0.js"></script>
    <script src="../../Scripts/bootstrap.js"></script>
</body>
</html>
