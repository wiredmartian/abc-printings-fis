﻿using ABCPrintingApp.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ABCPrintingApp.Pages.Product
{
    public partial class Details : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public Models.Product GetProduct([QueryString("productID")]int ProductId)
        {
            ProductsLogic l = new ProductsLogic();
            return l.GetProduct(ProductId);
        }
    }
}