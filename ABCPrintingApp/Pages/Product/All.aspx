﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="All.aspx.cs" Inherits="ABCPrintingApp.Pages.Product.All" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ABC Printing</title>
    <link href="../../Content/bootstrap.css" rel="stylesheet" />
    <link href="../../Content/bootstrap-theme.css" rel="stylesheet" />
    </head>
<body class="container">
    <div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">ABC Printing</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="All.aspx">Products</a></li>
              <li><a href="Add.aspx">Add</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
        </div>
    <form id="form1" runat="server">
        <h2> All Products</h2>
        <asp:GridView ID="gvProducts" runat="server" Height="169px" Width="716px" class="table table-bordered table-hover">
            <Columns>
                <asp:HyperLinkField DataNavigateUrlFields="ProductID" DataNavigateUrlFormatString="/Pages/Product/Details.aspx?productID={0}" HeaderText="View" Text="View Product" ControlStyle-CssClass="btn btn-primary" />
            </Columns>
        </asp:GridView>
    <br />

        

    </form>
    <script src="../../Scripts/jquery-1.9.0.js"></script>
    <script src="../../Scripts/bootstrap.js"></script>
</body>
</html>
