﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Details.aspx.cs" Inherits="ABCPrintingApp.Pages.Product.Details" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ABC Printing</title>
    <link href="../../Content/bootstrap.css" rel="stylesheet" />
    <link href="../../Content/bootstrap-theme.css" rel="stylesheet" />
</head>
<body class="container">
    <form id="form1" runat="server">
          <div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">ABC Printing</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="All.aspx">Products</a></li>
              <li><a href="Add.aspx">Add</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
        </div>
    <div>
    <asp:FormView ID="productDetail" runat="server" ItemType="ABCPrintingApp.Models.Product" SelectMethod ="GetProduct" RenderOuterTable="false">
        <ItemTemplate>
            <div>
                <h1><%#:Item.Name %></h1>
            </div>
            <br />
            <table>
                <tr>
                    <td>
                        <img src="data:image/jpeg;base64,<%#: Convert.ToBase64String(Item.Image) %>" style="height:300px" alt="<%#:Item.Name %>" class="img-responsive img-thumbnail"/>
                    </td>
                    <td>&nbsp;</td>  
                    <td style="vertical-align: top; text-align:left;">
                        <b>Color:</b><%#:Item.Color %><br /><b>Description:</b><%#:Item.Description %><br /><span><b>Price:</b>&nbsp;<%#: String.Format("{0:c}", Item.Price) %></span><br /><span><b>Product Number:</b>&nbsp;<%#:Item.ProductID %></span><br /><span><b>Available Qty:</b>&nbsp;<%#:Item.Available_QTY %></span><br /><br />
                        <span><a class="btn btn-primary" href="PlaceOrder.aspx?ProductId=<%#: Item.ProductID %>">Order Item</a></span><br />
                    </td>
                    
                </tr>
            </table>
        </ItemTemplate>
    </asp:FormView>
    </div>
    </form>
    <script src="../../Scripts/jquery-1.9.0.js"></script>
    <script src="../../Scripts/bootstrap.js"></script>
</body>
</html>
