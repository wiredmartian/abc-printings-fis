﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PlaceOrder.aspx.cs" Inherits="ABCPrintingApp.Pages.Product.PlaceOrder" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ABC Printing</title>
    <link href="../../Content/bootstrap.css" rel="stylesheet" />
    <link href="../../Content/bootstrap-theme.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            width: 249px;
        }
        .auto-style3 {
            width: 249px;
            height: 40px;
        }
        .auto-style4 {
            height: 40px;
        }
    </style>
</head>
<body class="container">
      <div class="container">
    <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">ABC Printing</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li class="active"><a href="All.aspx">Products</a></li>
              <li><a href="Add.aspx">Add</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
        </div>
    <form id="form1" runat="server">
        <h2>Place Order</h2>
    <div>
    

        <table class="auto-style1">
            <tr>
                <td class="auto-style2">Print Text</td>
                <td>
                    <asp:TextBox ID="txtPrint" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Quantity</td>
                <td>
                    <asp:TextBox ID="txtQty" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Your Phone</td>
                <td>
                    <asp:TextBox ID="txtNumber" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">Your Email</td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td>
                    <asp:Button ID="btnOrder" runat="server" CssClass="btn btn-default" Height="42px" Text="Order" Width="143px" OnClick="btnOrder_Click" />
                    <asp:Button ID="btnTotal" runat="server" CssClass="btn btn-primary" Height="40px" OnClick="btnTotal_Click" Text="Calculate " Width="141px" />
                </td>
            </tr>
            <tr>
                <td class="auto-style3"></td>
                <td class="auto-style4">
                    <asp:Label ID="lblTotal" runat="server"></asp:Label>
                </td>
            </tr>
        </table>
    

    </div>
    </form>
    <script src="../../Scripts/jquery-1.9.0.js"></script>
    <script src="../../Scripts/bootstrap.js"></script>
</body>
</html>
