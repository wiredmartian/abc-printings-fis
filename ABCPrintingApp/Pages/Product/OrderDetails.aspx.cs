﻿using ABCPrintingApp.Business;
using ABCPrintingApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ABCPrintingApp.Pages.Product
{
    public partial class OrderDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public Order GetOrder([QueryString("OrderId")]int OrderId)
        {
            ProductsLogic l = new ProductsLogic();
            return l.GetOrder(OrderId);
        }
    }
}