﻿using System;
using System.Collections.Generic;
using ABCPrintingApp.Models;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ABCPrintingApp.Business;

namespace ABCPrintingApp.Pages.Product
{
    public partial class All : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ProductsLogic logic = new ProductsLogic();
                gvProducts.DataSource = logic.GetAllProducts();
                gvProducts.DataBind();
            }
        }
        public List<Models.Product> GetProducts()
        {
            ProductsLogic logic = new ProductsLogic();
            return logic.GetAllProducts();
        }

        protected void btnAll_Click(object sender, EventArgs e)
        {
            ProductsLogic logic = new ProductsLogic();
            //gdProducts.DataSource = logic.GetAllProducts();
        }
    }
}