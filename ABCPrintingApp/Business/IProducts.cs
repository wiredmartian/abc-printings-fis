﻿using ABCPrintingApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ABCPrintingApp.Business
{
    public interface IProducts
    {
        string AddProduct(Product product, HttpPostedFile image);
        string RemoveProduct(int productID);
        List<Product> GetAllProducts();
        Product GetProduct(int productID);
        byte[] ConvertToBytes(HttpPostedFile file);

    }
}
