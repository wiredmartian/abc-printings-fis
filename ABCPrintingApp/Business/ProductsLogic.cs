﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ABCPrintingApp.Models;
using System.IO;
using System.Data.Entity;

namespace ABCPrintingApp.Business
{
    public class ProductsLogic : IProducts
    {
        private ABCEntities _db = new ABCEntities();
        public string AddProduct(Product product, HttpPostedFile image)
        {
            string response = "Unable to save product!";
            if(image != null)
            {
                product.Image = ConvertToBytes(image);
                _db.Products.Add(product);
                _db.SaveChanges();
                response = "Stored successfully!";
            }
            return response;
        }

        public byte[] ConvertToBytes(HttpPostedFile file)
        {
            BinaryReader reader = new BinaryReader(file.InputStream);
            return reader.ReadBytes((int)file.ContentLength);
        }

        public List<Product> GetAllProducts()
        {
            return _db.Products.ToList();
        }

        public Product GetProduct(int productID)
        {
            Product p = _db.Products
                .FirstOrDefault(x => x.ProductID == productID);
            return (p == null) ? null : p;
        }

        public string RemoveProduct(int productID)
        {
            throw new NotImplementedException();
        }



        /* Order */

        public Order PlaceOrder(Order order, int productId)
        {
            Product p = _db.Products
                .FirstOrDefault(x => x.ProductID == productId);
            if(p != null)
            {
                decimal total =  (p.Price * order.Quantity) + (10 * order.Shirt_Wrting.Length * order.Quantity);

                order.ProductID = p.ProductID;
                order.OrderDate = DateTime.Now;
                order.Total_Price = total;
                _db.Orders.Add(order);
                _db.SaveChanges();
                return order;
            }
            return null;
        }
        public Order GetOrder(int OrderId)
        {
            Order order = _db.Orders
                .Include(p => p.Product)
                .FirstOrDefault(p => p.OrderID == OrderId);
            return order;
        }
        public decimal CalcTotal(int qty, int characters, int productId)
        {
            Product p = _db.Products
                .FirstOrDefault(x => x.ProductID == productId);
            if (p != null)
            {
                decimal total = (p.Price * qty) + (10 * characters * qty); /*Each character is 10 * Quantity*/
                return total;
            }
            return 0; /*Null product*/
        }
    }
}